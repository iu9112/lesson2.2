const express = require("express");
const bodyParser = require("body-parser");
const PORT = 3000;
const app = express();
const rtAPIv1 = express.Router();
let user = {};
let users = [
	{ 
		'name': 'Alex',
		'score': '15'
	},
	{ 
		'name': 'Sergey',
		'score': '156'
	},
	{ 
		'name': 'Petr',
		'score': '200'
	},
];
rtAPIv1.use(bodyParser.urlencoded({ extended: true }));
rtAPIv1.get("/users/", function(req, res) {
res.send(users);
});
rtAPIv1.post('/users/', function(req, res) {
	user.name = req.body.name;
	user.score = req.body.score;
	users.push(user);
	user = {};
    res.send(users);
});
rtAPIv1.use('/users/:id', function (req, res, next) {
  console.log('Request Type:', req.method);
  next();
});
rtAPIv1.get("/users/:id", function(req, res) {
	res.send(users[req.params.id]);
});
rtAPIv1.put("/users/:id", function(req, res) { 
	users[req.params.id].name = req.body.name;
	users[req.params.id].score = req.body.score;
	res.send(users);
});
rtAPIv1.delete("/users/:id", function(req, res) { 
	users.splice(req.params.id, 1);
	res.send(users);
});
app.listen(PORT, () => {
  console.log('Hello! We are live on ' + PORT);
});
app.use("/api/v1", rtAPIv1);